'use strict';

$(window).on('load', function() {
		
	jQuery(window).on('scroll', function(){
	 	if(jQuery(window).scrollTop()){
	 		jQuery('header').addClass('fixed-it')
	 	}
	 	else{
	 		jQuery('header').removeClass('fixed-it')
	 	}
	})

	$(".navbar-toggler").on('click', function() {
	    $(".content-pt").toggleClass("blur-it");
	    $("header").toggleClass("opacity-it");
	});

	$('.slider-wrapper').slick({
	  	  dots: false,
        arrows:false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        variableWidth: false,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
              	arrows:false,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
              	arrows:false,
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
              	arrows:false,
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
	});

	$('.slide-it').slick({
	  	dots: true,
	    arrows:true,
	    infinite: true,
	    autoplay: false,
	    centerMode:false,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    variableWidth: false,
	    fade: false,
	    responsive: [
            {
              breakpoint: 1024,
              settings: {
                arrows: false
              }
            },
            {
              breakpoint: 600,
              settings: {
                arrows: false
              }
            },
            {
              breakpoint: 480,
              settings: {
                arrows: false
              }
            }
          ]
	});


	$('.slide-logo').slick({
        dots: false,
        arrows:true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        variableWidth: false,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                arrows:false,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
              	arrows:false,
              	dots: true,
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
              	arrows:false,
              	dots: true,
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    });

	$('.skins-slider').slick({
	  	dots: true,
	    arrows:false,
	    infinite: true,
	    autoplay: false,
	    slidesToShow: 3,
	    slidesToScroll: 1,
	    variableWidth: false,
	    fade: false,
	    centerMode: true,
	    centerPadding: '0px',
	    responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
	});

	$('.how-to-work-slider').slick({
	  	dots: true,
	    arrows:false,
	    infinite: true,
	    autoplay:false,
	    slidesToShow: 3,
	    slidesToScroll: 1,
	    variableWidth: false,
	    centerMode: true,
	    centerPadding: '0px',
	    fade: false,
	    responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
	});

	$('.blog-slider').slick({
	  	dots: true,
	    arrows:false,
	    infinite: true,
	    autoplay:true,
	    centerMode:false,
	    slidesToShow: 3,
	    slidesToScroll: 1,
	    variableWidth: false,
	    fade: false,
	    responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
	});

}); 



$(function() {

  var gate = $(window),
  mid = gate.width()/15,
  hub = gate.height()/30;

  $('.hero-pt').mousemove(function(e) {

    var place = mid-e.pageX, spot = hub-e.pageY;

    $(this).find('.hero-images .hero-image .img2').each(function() {

      $(this).css('transform', 'translate(' + 0.03*place + 'px, ' + 0.03*spot + 'px)');
    });

    $(this).find('.hero-images .hero-image .img3').each(function() {

      $(this).css('transform', 'translate(' + 0.02*place + 'px, ' + 0.03*spot + 'px)');
    });

    $(this).find('.easy-feature-image .easy-one').each(function() {

      $(this).css('transform', 'translate(' + 0.01*place + 'px, ' + 0.09*spot + 'px)');
    });
  });
});



