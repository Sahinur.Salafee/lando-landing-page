let mix = require('laravel-mix');

mix.sass('sass/main.scss', 'dist/css')
.options({
	processCssUrls:false
});